package main

import (
	"log"
	"testing"

	"gitlab.com/lasaleks/svsignal/model"
)

func TestMigrate(t *testing.T) {
	cfg.SVSIGNAL.SQLite = &struct {
		FILE   string   `yaml:"file"`
		PRAGMA []string `yaml:"pragma"`
	}{
		FILE: ":memory:",
	}
	connectDataBase()
	if cfg.SVSIGNAL.DEBUG_LEVEL >= 6 {
		DB = DB.Debug()
	}
	sqlDB, err := DB.DB()
	if err != nil {
		log.Panicln("DB err:", err)
	}
	defer sqlDB.Close()

	// migrate DB
	if err := model.Migrate(DB); err != nil {
		t.Error("model.Migrate err:", err)
	}
}
