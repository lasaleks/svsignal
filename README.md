docker run --name=svsignal -i --rm \
-v $path/sock:/sock \
-v $path/svsignal_db:/app/svsignal/database \
-e TIME_ZONE="Asia/Novokuznetsk" \
--net=net-ie \
--ip 172.19.0.144 \
--log-driver json-file \
--log-opt max-size=10m \
--log-opt max-file=10 \
$image
