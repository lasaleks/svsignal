import time
import json
import requests
from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings
from django.shortcuts import render


def index(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/login/?next=/svsignal/")
    return render(request, 'svsignal/trend.html', context={})

