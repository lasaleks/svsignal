function strDate2Date(str_date) {
  let _date = str_date.split(' ')[0];
  let _time = str_date.split(' ')[1];
  let day = parseInt(_date.split('.')[0]), mount = parseInt(_date.split('.')[1])-1, year = parseInt(_date.split('.')[2]);
  let hour = parseInt(_time.split(':')[0]), minute = parseInt(_time.split(':')[1]), second = parseInt(_time.split(':')[2]);
  return new Date(year, mount, day, hour, minute, second);
}
let cur = null;

class Trend {
  charts = [];
  signals = {};
  begin = 0;
  end = 0;
  title = '';
  height = '80vh';
  columns = 1;
  lang = 'ru';
  modalLoading = null;
  modalLoadingText = null;
  locale = {
    name: 'ru',
    options: {
        months: [ 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' ],
        shortMonths: [ 'Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек' ],
        days: [ 'Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота' ],
        shortDays: [ 'Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб' ],
        toolbar: {
            exportToSVG: 'Сохранить SVG',
            exportToPNG: 'Сохранить PNG',
            exportToCSV: 'Сохранить CSV',
            menu: 'Меню',
            selection: 'Выбор',
            selectionZoom: 'Выбор с увеличением',
            zoomIn: 'Увеличить',
            zoomOut: 'Уменьшить',
            pan: 'Перемещение',
            reset: 'Сбросить увеличение'
        }
    }
  };
  list_signals2find = {};
  listSigData = {};
  constructor() {
    let dt = new Date();
    this.begin = dt.setHours(0, 0, 0, 0)/1000;
    dt.setDate(dt.getDate()+1);
    this.end = dt.setHours(0, 0, 0, 0)/1000;
    this.decodeParams();
  }
  async getLocale(lang) {
    let resp = await fetch(`/static/apexcharts/locales/${lang}.json`);
    if(resp.ok) {
      this.locale = await resp.json();
    } else {
      console.log(`ERROR load apexcharts ${lang} locales`, resp);
    }
  }
  decodeParams() {
    let urlParams = new URLSearchParams(window.location.search);
    const params = Object.fromEntries(urlParams.entries());
    if('begin' in params) this.begin = params.begin; 
    if('end' in params) this.end = params.end;
    if('title' in params) {
      let title = document.querySelector('header div[style="flex: 1 1 auto;"]');
      if(title) title.textContent = `График: ${params.title}`;
      this.title = params.title;
    }
    if('height' in params) this.height = `${params.height}vh`;
    if('cols' in params) this.columns = parseInt(params.cols) > 3 ? 3 : parseInt(params.cols);
    if('lang' in params) this.lang = params.lang;
    if('signals' in params) {
      let trendsSignals = params.signals.replaceAll(' ', '').split(/\[|\]|$/);
      let chart_idx = 0;
      for(let idx=0; idx<trendsSignals.length; idx++) {
        if(trendsSignals[idx] == '' || trendsSignals[idx] == ',') continue;
        let chart = { signals: [], idx: chart_idx, obj: null, div: null, id: `chart_${chart_idx}`, options: {}, autoZoom: false };
        let tmp = trendsSignals[idx].split(',');
        for(let i=0;i<tmp.length; i++) {
          let signalKey = tmp[i];
          if(!this.signals.hasOwnProperty(signalKey))
            this.signals[signalKey] = {
              signalKey: signalKey,
              color: '#000',
              name: '',
              data: [],
              y_min: 0,
              y_max: 0,
              y_fixed: 1,
              data_loaded: false
            }
          chart.signals.push(signalKey);
        }
        this.charts.push(chart);
        chart_idx += 1;
      }
    }
    document.querySelector('#btnQuery').addEventListener('click', this.getData.bind(this));
  }
  async init() {
    this.modalLoading = document.querySelector('div.modal.mrs-loading');
    this.modalLoadingText = this.modalLoading.querySelector('span.mrs-loading-text');
  
    let div_charts = document.querySelector('#div_charts');
    if(!div_charts) return;

    let tree = document.querySelector('#listSignals');
    while (tree.firstChild) tree.removeChild(tree.firstChild);

    if(Object.keys(this.signals).length == 0) {
      let btnList = document.querySelector('#btnList');
      btnList.addEventListener('click', this.listSignals.bind(this));
      btnList.style.display = '';
      btnList.click();
      this.charts.push({ signals: [], idx: 0, obj: null, div: null, id: 'chart_0', options: null, autoZoom: false });
      this.columns = 1;

      this.modalLoadingText.textContent = 'Загрузка списка сигналов...';
      $(this.modalLoading).modal('show');
      let resp = await fetch('/svs/api/getlistsignal');
      if(resp.ok) {
        this.listSigData = await resp.json();
        this.modalLoadingText.textContent = 'Построение списка сигналов...';
        let self = this;
        function makeNodeSignal(parent, node) {
          let el_li = document.createElement('li');
          el_li.classList.add('trend-item', 'pointer');
          el_li.dataset['signal'] = node.fullKey;
          el_li.dataset['type_save'] = node.typesave;
          el_li.dataset['name'] = node.name;
          el_li.dataset['show'] = 1;
          el_li.innerHTML = `
          <i class="far fa-square signal-name"></i>
          <span class="signal-name">${node.name}</span>
          <!--div style="float: right; padding-left: .5rem">
            <i class="fas fa-cog signal-settings"></i>
          </div-->`;
          parent.appendChild(el_li);
          node.node = el_li;
        }
        function makeNodeTree(parent, node) {
          let el_ul = document.createElement('ul');
          el_ul.classList.add('treeNode');
          let el_li = document.createElement('li');
          let el_details = document.createElement('details');
          el_details.classList.add('trendList');
          el_li.appendChild(el_details);
          let el_summary = document.createElement('summary');
          el_summary.classList.add('trendList');
          el_details.appendChild(el_summary);
          let el_span_name = document.createElement('span');
          el_span_name.textContent = node.name;
          el_summary.appendChild(el_span_name);
          let el_ul_details = document.createElement('ul');
          node.node = el_ul;
          for (const [key, childNode] of Object.entries(node.childNodes)) {
            if(childNode.hasOwnProperty('childNodes'))
              makeNodeTree(el_details, childNode);
            else 
              makeNodeSignal(el_ul_details, childNode);
          }
          el_details.appendChild(el_ul_details);
          el_ul.appendChild(el_li);
          parent.appendChild(el_ul);
        }
        for (const [key_system, system] of Object.entries(this.listSigData)) {
          let systemNode = this.list_signals2find[key_system] = {
            name: system.name == undefined || system.name == '' ? key_system : system.name, 
            node: null,
            visible: true,
            childNodes: {},
            keys_string: [],
            add_keys: function(list_keys) {
              if(!list_keys) return;
              list_keys.forEach(str => {
                if(this.keys_string.includes(str) == false) this.keys_string.push(str);
              })
            }
          };
          for (const [key_signal, signal] of Object.entries(system.signals)) {
            let groups = [];
            if(signal.hasOwnProperty('tags')) {
              for(let idx=0; idx<signal.tags.length; idx++) {
                if(signal.tags[idx].hasOwnProperty('tag') && signal.tags[idx].tag == 'site') {
                  if(signal.tags[idx].value in groups == false) groups.push(signal.tags[idx].value);
                  break;
                }
              }
            }
            // не найден тег "site", поэтому разбиваем key_signal по символу "."
            if(groups.length == 0) {
              let arr = key_signal.split('.');
              groups = arr.slice(1, arr.length == 2 ? undefined : -1);
            }
            let parentNode = systemNode;
            let lastNode = null;
            for(let idx=0; idx<groups.length; idx++) {
              lastNode = parentNode.childNodes[groups[idx]];
              if(!lastNode) {
                lastNode = parentNode.childNodes[groups[idx]] = {
                  parent: parentNode,
                  name: groups[idx],
                  node: null,
                  visible: true,
                  childNodes: {},
                  keys_string: [groups[idx]],
                  add_keys: function(list_keys) {
                    if(!list_keys) return;
                    list_keys.forEach(str => {
                      if(this.keys_string.includes(str) == false) this.keys_string.push(str);
                    })
                  }
                }
              }
              parentNode = lastNode;
            }
            let nodeSignal = lastNode.childNodes[key_signal] = {
              parent: lastNode,
              name: signal.name,
              node: null,
              fullKey: key_signal,
              typesave: signal.typesave,
              visible: true,
              keys_string: [key_signal, signal.name]
            }
            lastNode.add_keys(nodeSignal.keys_string);
            while(lastNode.parent) {
              lastNode.parent.add_keys(lastNode.keys_string);
              lastNode = lastNode.parent;
            }
          }
          makeNodeTree(tree, systemNode);
        }
      } else {
        console.log(`ERROR load apexcharts ${this.lang} locales`, resp);
      }
      let li_signals = tree.querySelectorAll('li.trend-item');
      for (let idx=0; idx<li_signals.length; idx++)
        li_signals[idx].addEventListener('click', this.checkTrend.bind(this, li_signals[idx]));

      let findSignal = document.querySelector('#findSignal');
      findSignal.addEventListener('input', this.filterSignals.bind(this));
      $(this.modalLoading).modal('hide');
    }
    let rows_count = Math.ceil(this.charts.length / this.columns);
    for(let row_idx=0; row_idx<rows_count; row_idx++) {
      let div_row = document.createElement('div');
      div_row.classList.add('row', 'm-0');
      div_row.id = `div_chart_row_${row_idx}`;
      div_row.style.height = this.height;
      for(let idx=row_idx*this.columns; idx<row_idx*this.columns+this.columns; idx++) {
        if(idx > this.charts.length-1) continue;        
        let div_col = document.createElement('div')
        div_col.classList.add(`col-sm-${12/this.columns}`, 'p-0', 'px-1');
        div_row.appendChild(div_col);
        let div_chart = document.createElement('div');
        div_chart.id = `chart_${idx}`;
        this.charts[idx].div = div_chart;
        this.charts[idx].options = {
          series: [],
          chart: { 
            type: 'line', 
            height: '100%', 
            width: '100%', 
            toolbar: { 
              tools: {
                customIcons: [{
                  icon: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="transform: scale(.85);"><path d="M0 0h24v24H0z" fill="none" class="zoom_a"></path><path d="M1,5h4v-4ZM1,19h4v4ZM19,23v-4h4ZM19,1v4h4ZM6,6h12v12h-12zM8,8h8v8h-8z" style="fill-rule: evenodd"></path></svg>',
                  index: 4,
                  title: 'Автомасштабирование',
                  class: 'autoZoom',
                  click: this.autozoom.bind(this)
                }]
              }
            }, 
            defaultLocale: this.lang, locales: [this.locale], 
            animations: { enabled: false },
            id: `apexchart_${idx}`,
            group: 'trends',
          },
          stroke: { width: 2 },
          dataLabels: { enabled: false },
          legend: { show: true, showForSingleSeries: true },
          xaxis: { 
            type: 'datetime', 
            labels: { datetimeUTC: false }, 
            tooltip: { enabled: false },
            min: this.begin * 1000,
            max: this.end * 1000,
            title: { text: 'Время' },
          },
          yaxis: {
            autoZoom: false,
            minYvalue: null,
            maxYvalue: null,
            minY: null,
            maxY: null,
            showAlways: true,
            min: function(val) {
              return this.autoZoom ? this.minYvalue : this.minY;
            },
            max: function(val) {
              return this.autoZoom ? this.maxYvalue : this.maxY;
            }
          },
          tooltip: { 
            theme: 'dark',
            x: { format: 'dd.MM.yyyy HH:mm:ss' }
          },
          grid: {
            row: { colors: ['#e5e5e5', 'transparent'], opacity: 0.3 },
            column: { colors: ['#f8f8f8', 'transparent'] },
            xaxis: { lines: { show: true } }
          },
          /*noData: {
            text: Object.keys(this.signals).length > 0 ? 'Загрузка данных...' : 'Не выбран сигнал...'
          }*/
        }
        div_col.appendChild(div_chart);        
      }
      div_charts.appendChild(div_row);
    }
    for(let idx=0; idx<this.charts.length; idx++) {
      let chart = this.charts[idx];
      chart.obj = new ApexCharts(chart.div, chart.options);
      chart.obj.render();
    }
  }
  async getData() {
    this.modalLoadingText.textContent = 'Загрузка данных по сигналам...';
    $(this.modalLoading).modal('show');
    for (const [key, signal] of Object.entries(this.signals)) {
      let responce = await fetch(`/svs/api/signal/getdata?signalkey=${key}&begin=${this.begin}&end=${this.end}`);
      if(responce.ok) {
        let data = await responce.json();
        data.tags.forEach(item => {
          switch(item.tag) {
            case 'min_y': { signal.y_min = parseFloat(item.value); } break;
            case 'max_y': { signal.y_max = parseFloat(item.value); } break;
            case 'series_color': { signal.color = item.value; } break;
            case 'y_fixed': { signal.y_fixed = item.value; } break;
          }
        });
        signal.name = data.signalname;
        let utime = Math.floor(new Date().getTime()/1000);
        signal.data = [];
        signal.y_max_values = null;
        signal.y_min_values = null;

        switch(data.typesave) {
          case 1: {
            if(data.values.length == 1) { 
              signal.y_max_values = data.values[0][2];
              signal.y_min_values = data.values[0][2];
              if(data.values[0][1] < this.begin)
                signal.data.push([this.begin * 1000, data.values[0][2]]);
              signal.data.push([this.end < utime ? this.end * 1000 : utime * 1000, data.values[0][2]]);
            } else {
              let prev_point = null;
              for (let idx=0; idx<data.values.length; idx++) {
                let point = [data.values[idx][1] * 1000, data.values[idx][2]];
                if(!signal.y_max_values || data.values[idx][2] > signal.y_max_values) signal.y_max_values = data.values[idx][2];
                else if(!signal.y_min_values || data.values[idx][2] < signal.y_min_values) signal.y_min_values = data.values[idx][2];

                // превая точка
                if(prev_point == null) {
                  // установка точки в начало графика
                  if(data.values[idx][1] < this.begin)
                    point = [this.begin * 1000, data.values[idx][2]];
                  prev_point = point;
                  signal.data.push(point);
                  continue;
                }
                // установка точки в конец графика
                if(data.values[idx][1] > this.end) {
                  point = [this.end * 1000, data.values[idx][2]];
                  prev_point = point
                  signal.data.push(point);
                  break;
                }
                if(prev_point[1] != point[1] && prev_point[1] != point[2])
                  signal.data.push([point[0], prev_point[1]]);
                signal.data.push(point);
                prev_point = point;
              }
            }
          } break;
          case 2: {
            data.values.forEach(rec => {
              signal.data.push([rec[1] * 1000, rec[2]]);
              if(signal.y_max_values == null || rec[2] > signal.y_max_values) signal.y_max_values = rec[2];
              else if(signal.y_min_values == null || rec[2] < signal.y_min_values) signal.y_min_values = rec[2];
            });
          } break;
        }
        signal.data_loaded = true;
        signal.y_max_values = Math.ceil(signal.y_max_values * (signal.y_max_values > 0 ? 1.05 : 0.95));
        signal.y_min_values = Math.ceil(signal.y_min_values * (signal.y_min_values > 0 ? 0.95 : 1.05));
      }
    }
    // обновляем данные на графиках
    for(let idx=0; idx<this.charts.length; idx++) {
      let chart = this.charts[idx];
      let chart_series = [];
      let y_min = null, y_max = null, y_min_values = null, y_max_values = null, y_fixed = 1;
      for(let sig_idx=0; sig_idx<chart.signals.length; sig_idx++) {
        let signal = this.signals[chart.signals[sig_idx]];
        chart_series.push({name: signal.name, data: signal.data});
        y_min = y_min == null || y_min > signal.y_min ? signal.y_min : y_min;
        y_max = y_max == null || y_max < signal.y_max ? signal.y_max : y_max;
        y_min_values = y_min_values == null || y_min_values > signal.y_min_values ? signal.y_min_values : y_min_values;
        y_max_values = y_max_values == null || y_max_values < signal.y_max_values ? signal.y_max_values : y_max_values;
        if(signal.y_fixed && signal.y_fixed > y_fixed) 
          y_fixed = signal.y_fixed;
      }
      chart.options.series = chart_series;  
      chart.obj.updateSeries(chart_series);
      chart.options.xaxis.min = this.begin * 1000;
      chart.options.xaxis.max = this.end * 1000;
      chart.options.yaxis[0].minY = y_min;
      chart.options.yaxis[0].maxY = y_max;
      chart.options.yaxis[0].minYvalue = y_min_values;
      chart.options.yaxis[0].maxYvalue = y_max_values;
      chart.options.yaxis[0].decimalsInFloat = y_fixed;
      chart.obj.updateOptions(chart.options, false, true, false);
    }
    $(this.modalLoading).modal('hide');
  }
  async checkTrend(el, event) {
    // console.log(event, this, el, event.target);
    if(event.target.classList.contains('signal-name')) {
      let i_check = el.querySelector('i.signal-name');
      if(this.signals.hasOwnProperty(el.dataset.signal)) {
        delete this.signals[el.dataset.signal];
        for(let idx=0; idx<this.charts[0].signals.length; idx++) {
          if(this.charts[0].signals[idx] == el.dataset.signal) {
            this.charts[0].signals.splice(idx, 1);
            break;
          }
        }
      }
      else {
        this.signals[el.dataset.signal] = { signalKey: el.dataset.signal, color: '#000', name: '', data: [], y_min: 0, y_max: 0, y_fixed: 0, data_loaded: false };
        this.charts[0].signals.push(el.dataset.signal);
      }
      i_check.classList.toggle('fa-check-square');
      i_check.classList.toggle('fa-square');
      await this.getData();
    }
    else if(event.target.classList.contains('signal-settings')) {
      // тыкнули на шестерёнку настройки - открываем модальное окно с настройками по сигналу, по закритии обновляем отображение графика

    }
  }
  listSignals(event) {
    let el = event.target;
    if(el.tagName == "I") el = el.parentElement;
    el.classList.toggle('active');
    if(el) {
      el.ariaPressed = `${el.classList.contains('active')}`;
      document.querySelector('#signals').style.display = el.classList.contains('active') ? '' : 'none';
      document.querySelector('#div_resize').style.display = el.classList.contains('active') ? '' : 'none';
    }
  }
  filterSignals(event) {
    let sfilter = event.target.value.toLowerCase();
    // console.log(this, event, event.target.value, sfilter);
    function checkFilter(item) {
      let hideNode = true;
      for(let idx=0;idx<item.keys_string.length; idx++) {
        if(item.keys_string[idx].indexOf(sfilter) != -1) {
          hideNode = false;
          break;
        }
      }
      if(!hideNode && item.hasOwnProperty('childNodes')) {
        for (const [key, node] of Object.entries(item.childNodes)) {
          checkFilter(node);
        }
      }  
      item.visible = !hideNode;
      item.node.style.display = hideNode ? 'none' : '';
    }
    for (const [key, item] of Object.entries(this.list_signals2find)) {
      checkFilter(item);
    }
  }
  autozoom(chart, options, event) {
    // console.log("clicked custom-icon", chart, options, event, event.target);
    for(let idx=0; idx<this.charts.length; idx++) {
      let chart = this.charts[idx];
      if(chart.options.chart.id == options.globals.chartID) {
        chart.options.yaxis[0].autoZoom = !chart.options.yaxis[0].autoZoom;
        chart.obj.updateOptions(chart.options, false, true, false).then((val) => {
          let el = document.querySelector(`#chart_${idx} path.zoom_a`);
          el.setAttribute('fill', chart.options.yaxis[0].autoZoom ? '#95cffb' : 'none');  
        });
        break;
      }
    }
  }
}
function resizeWindowTrend() {
  let div_mainStyle = getComputedStyle(document.querySelector('div.content div.content-fluid'));
  
  let div_periodStyle = getComputedStyle(document.querySelector('#div_period'));
  let div_signals = document.querySelector('#signals');
  div_signals.parentElement.style.height = `calc(${div_mainStyle.height} - ${div_periodStyle.height} - 0.6rem)`;

  let div_signalsStyle = getComputedStyle(div_signals);

  let div_findStyle = getComputedStyle(document.querySelector('#findSignal'));
  let div_listSignals = document.querySelector('#listSignals');
  div_listSignals.style.height = `calc(${div_signalsStyle.height} - ${div_findStyle.height} - 0.6rem)`;
}

function start_resize(e) {
  let el = document.querySelector('#signals');
  cur = {el: el, width: parseFloat(getComputedStyle(el).width), x: e.clientX};
}
document.onmouseup = function() { if(cur) cur = null; };
document.onmousemove = function(e) {
  if(!cur) return;
  with(cur) {
    let nx = width + x - e.clientX;
    el.style.width = `${nx}px`;
  }
  e.preventDefault();
}
document.ondragstart = function() { return false; }

document.addEventListener('readystatechange', async () => {
  switch(document.readyState) {
    case 'interactive': {
      window['trend'] = null;
    } break;
    case 'complete': {
      let tmp = null;


      jQuery.datetimepicker.setLocale('ru');
      trendOBJ = new Trend();
      if(trendOBJ.lang != 'ru') {
        await trendOBJ.getLocale(trendOBJ.lang);
        jQuery.datetimepicker.setLocale(trendOBJ.lang);
      }
      await trendOBJ.init();
      $('#dtp_begin').datetimepicker({
        step:10,
        mask:true,
        format:'d.m.Y H:i:s',
        onChangeDateTime:function(dp, $input) {
          trendOBJ.begin = strDate2Date($input.val()).getTime()/1000;
        }
      });
      tmp = document.querySelector('#dtp_begin');
      if(tmp) {
        let dt = new Date(trendOBJ.begin * 1000);
        tmp.value = dt.toLocaleString().replace(',', '');
      }
      $('#dtp_end').datetimepicker({
        step:10,
        mask:true,
        format:'d.m.Y H:i:s',
        onChangeDateTime:function(dp, $input) {
          trendOBJ.end = strDate2Date($input.val()).getTime()/1000;
        }
      });
      tmp = document.querySelector('#dtp_end');
      if(tmp) {
        let dt = new Date(trendOBJ.end * 1000);
        tmp.value = dt.toLocaleString().replace(',', '');
      }
      window.addEventListener("resize", resizeWindowTrend);
      resizeWindowTrend();
      await trendOBJ.getData();

    } break;
  }
});
// http://localhost/svsignal/?signals=[beacon.1794.di1],[beacon.1794.rx]&cols=2&begin=1705338000&end=1705424400&cols=2
