# svsignal_py

[![PyPI - Version](https://img.shields.io/pypi/v/svsignal-py.svg)](https://pypi.org/project/svsignal-py)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/svsignal-py.svg)](https://pypi.org/project/svsignal-py)

-----

**Table of Contents**

- [Installation](#installation)
- [License](#license)

## Installation

```console
pip install svsignal-py
```

## License

`svsignal-py` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.
