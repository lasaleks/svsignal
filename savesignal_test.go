package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"sync"
	"testing"

	"github.com/lasaleks/gormq"
	"gitlab.com/lasaleks/svsignal/model"
)

func TestSetSignal(t *testing.T) {
	cfg.SVSIGNAL.SQLite = &struct {
		FILE   string   `yaml:"file"`
		PRAGMA []string `yaml:"pragma"`
	}{
		FILE: "file::memory:?cache=shared",
	}
	cfg.SVSIGNAL.DEBUG_LEVEL = 0
	connectDataBase()
	if cfg.SVSIGNAL.DEBUG_LEVEL >= 6 {
		DB = DB.Debug()
	}
	sqlDB, err := DB.DB()
	if err != nil {
		log.Panicln("DB err:", err)
	}
	defer sqlDB.Close()

	// migrate DB
	if err := model.Migrate(DB); err != nil {
		t.Error("model.Migrate err:", err)
	}
	res := DB.Create(&model.Group{Name: "asdfasdf", Key: "asdf"})
	fmt.Printf("%v", res.Error)

	var wg sync.WaitGroup
	ctx := context.Background()
	CH_MSG_AMPQ = make(chan gormq.MessageAmpq, 1)
	CH_SET_SIGNAL = make(chan SetSignal, 1)
	hub = newHub()
	hub.debug_level = cfg.SVSIGNAL.DEBUG_LEVEL
	ctx_hub, cancel_hub := context.WithCancel(ctx)

	setsig := SetSignal{
		TypeSave: TYPE_FVALUE,
		Period:   60,
		Delta:    5.0,
		Name:     "Качество связи TX/RX",
		Tags: []struct {
			Tag   string `json:"tag"`
			Value string `json:"value"`
		}{
			{Tag: "site", Value: "Выработка в.5-1-2"},
			{Tag: "max_y", Value: "110"},
			{Tag: "min_y", Value: "-5"},
			{Tag: "series_color", Value: "#000000"},
		},
	}

	jData, _ := json.Marshal(setsig)
	CH_MSG_AMPQ <- gormq.MessageAmpq{
		Exchange:     "svsignal",
		Routing_key:  "svs.set.beacon.1234.rx",
		Content_type: "text/plain",
		Data:         jData,
	}
	close(CH_MSG_AMPQ)
	wg.Add(1)
	hub.run(&wg, ctx_hub)
	close(CH_SET_SIGNAL)

	savesignal := newSVS()
	savesignal.Load()
	ctx_db, cancel_db := context.WithCancel(ctx)
	wg.Add(1)
	savesignal.Run(&wg, ctx_db)
	group := model.Group{}
	err = DB.Where(&model.Group{Key: "beacon"}).First(&group).Error
	if err != nil {
		t.Error("not found group key:beacon")
	}
	if group.Key != "beacon" {
		t.Error("not found group key:beacon")
	}

	signal := model.Signal{}
	err = DB.Where(&model.Signal{GroupID: group.ID, Key: "1234.rx"}).First(&signal).Error
	if err != nil {
		t.Errorf("not found signal %v", setsig)
	}

	if signal.Key != "1234.rx" {
		t.Errorf("not found signal %v", setsig)
	}

	if int(signal.TypeSave) != setsig.TypeSave &&
		signal.Period != setsig.Period &&
		signal.Delta != setsig.Delta &&
		signal.Name != setsig.Name {
		t.Error("not equal value")
	}

	for _, stag := range setsig.Tags {
		tag := model.Tag{}
		err = DB.Where(&model.Tag{SignalID: signal.ID, Tag: stag.Tag, Value: stag.Value}).First(&tag).Error
		if err != nil {
			t.Errorf("not found tag:%v", stag)
		}
	}

	cancel_db()
	cancel_hub()
	wg.Wait()
}
